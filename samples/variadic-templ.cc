// some tests for variadic templates


#include <iostream>
#include <string>

#include <tuple>
#include <type_traits>
#include <utility>
#include <initializer_list>

using std::cout;
using std::tuple;
using std::make_tuple;
using std::enable_if;

// end of recursion: print the rest
void myPrintfImpl(char const *rest)
{
    while (*rest)
    {
        if (*rest == '%')
        {
            ++rest;
        }
        cout << *rest++;
    }    
}
 
template<typename T, typename... Ts>
void myPrintfImpl(char const *fmt, T val, Ts... args)
{
    while (*fmt)
    {
        if (*fmt == '%')
        {
            ++fmt;
            if (*fmt != '%')
            {
                cout << val;
                myPrintfImpl(fmt, args...);
                return;
            }
        }
        cout << *fmt++;
    }    
}

template<typename... Ts>
inline void myPrintf(const std::string &fmt, Ts... args)
{
    myPrintfImpl(fmt.c_str(), args...);
}


// using enable_if to loop over tuple
// there's no standard function that gives the tail of a tupe
// so we always pass the full tuple (by reference)

// end of recursion: either an empty tuple or the iterator I is at the end
template <size_t I = 0, typename... Ts>
typename enable_if<sizeof...(Ts) == I>::type
tPrint(tuple<Ts...> &)
{
}

// standard print
template <size_t I = 0, typename... Ts>
typename enable_if<sizeof...(Ts) != I>::type
tPrint(tuple<Ts...> &tpl)
{
    // Ith element:
    cout << std::get<I>(tpl) << ", ";

    // recursion
    tPrint<I+1, Ts...>(tpl);
}

// stream with separator
class SepStream
{
public:
    SepStream(std::ostream &o, std::string_view sep)
      : out{o}
      , separator{sep}
    {}

    template <typename T>
    void print(T &&val)
    {
        out << std::forward<T>(val) << separator;
    }

private:
    std::ostream &out;
    std::string separator;
};

template <typename T>
SepStream &operator<<(SepStream &o, T &&val)
{
    o.print(std::forward<T>(val));
    return o;
}

// expand by folding
template <typename... Ts>
void printAny(Ts... args)
{
    (... << args); // ((arg0 << arg1) << arg2) << ...
}

template <typename T>
void printSingleAny(T &&val)
{
    cout << std::forward<T>(val) << ", ";
}

template <class Tp, size_t... i>
void printTupleHelper(Tp &&tpl, std::index_sequence<i...>)
{
    // forward:
    // if the tuple is an lvalue, the elements are lvalues as well
    // if the tuple is an rvalue, get will return an xvalue
    // so we need double forward: one for the tuple and one for the call
    // to printSingleAny

    // pack expansion into comma expression is not allowed
    // workaround: initializer_list
    (void)std::initializer_list<int>{(printSingleAny(std::forward<decltype(std::get<i>(tpl))>(std::get<i>(std::forward<Tp>(tpl)))), 0) ...};
//    (printSingleAny(std::forward<decltype(std::get<i>(tpl))>(std::get<i>(std::forward<Tp>(tpl)))) ...);
//(printSingleAny(std::get<i>(std::forward<Tp>(tpl))) ...);
}

template <class Tp>
void printTuple(Tp &&tpl)
{
    printTupleHelper(std::forward<Tp>(tpl), std::make_index_sequence<std::tuple_size_v<std::decay_t<Tp>>>());
}

// own make_tuple w/ leading element
// using pack expansion into comma separated list
// currently not useful
template <typename... Ts>
tuple<SepStream, Ts...> prependSepStream(Ts... args)
{
    return make_tuple(SepStream{cout, ", "}, args...);
}

int main()
{
    std::string s{"Hello!"};
    myPrintf("string: %, char *: %, double: %, int: % and a bare %%\n",
             s, "C++", 3.14159, 42);

    auto tpl = make_tuple('a', 37, "something", 2.5, "else");
    tPrint(tpl);
    cout << '\n';

    printTuple(tpl);
    cout << '\n';

    SepStream myOut{cout, ", "};
    printAny(myOut, 'a', 37, "something", 2.5, "else");
    cout << '\n';

#if 0
    // apply on a template function doesn't work
    // variadic templates probably would (still to test)
    auto x = prependSepStream(1, 27.7, 'a', "b");
    std::apply(printAny, std::apply(prependSepStream, tpl));
    cout << '\n';
#endif

    return 0;
}
