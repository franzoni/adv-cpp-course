// testing SFINAE

#include <iostream>

using std::cout;

void f1(int i)
{
    cout << "f(" << i << ")\n";
}
void f2(int *p)
{
    cout << "f(" << p << ")\n";
}

// TRY5, TR6 and TRY7 work
#define TRY7 1

// goal: a single function that calls f1() for int and f2() for int*
#if defined(TRY1)
// doesn't work as it's the same overload
template <typename T>
void f(T val)
{
    f1(val);
}

template <typename T>
void f(T val)
{
    f2(val);
}
#elif defined(TRY2)
// doesn't work as SFINAE only works in function prototypes
template <typename T>
void f(T val, int)
{
    f1(val); // this is a hard error, not a SFINAE failure
}

template <typename T>
void f(T val, ...)
{
    f2(val);
}
#elif defined(TRY3)
// doesn't work as default initializer is not part of type
template <typename T>
void f(T val, int = (f1(std::declval<T>()), 0)) // T() doesn't work either
{
    f1(val);
}

template <typename T>
void f(T val, ...)
{
    f2(val);
}
#elif defined(TRY4)
// doesn't work as f1() isn't constexpr
template <typename T>
void f(T val, int(*)[((f1(std::declval<T>())), 0)] = nullptr)
{
    f1(val);
}

template <typename T>
void f(T val, ...)
{
    f2(val);
}
#elif defined(TRY5)
// 'call' of f1() must be in unevaluated context
template <typename T>
void f(T val, decltype(f1(std::declval<T>())) * = nullptr)
{
    f1(val);
}

// we need to disambiguate, so we SFINAE out this one for ill-formed calls
// to f2()
template <typename T>
void f(T val, decltype(f2(std::declval<T>())) * = nullptr)
{
    f2(val);
}
#elif defined(TRY6)
// better make it the return type (as it fits)
template <typename T>
decltype(f1(std::declval<T>())) f(T val)
{
    f1(val);
}

template <typename T>
decltype(f2(std::declval<T>())) f(T val)
{
    f2(val);
}
#elif defined(TRY7)
// using (SFINAE) tag types
template <typename T> struct OnlyInt; // declaration only
template <> struct OnlyInt<int>   // specialization for int
{
    typedef void type;
};

template <typename T> struct OnlyPointer; // declaration only
template <typename T> struct OnlyPointer<T *> // partial specialization
{
    typedef void type;
};

template <typename T>
typename OnlyInt<T>::type f(T val)
{
    f1(val);
}

template <typename T>
typename OnlyPointer<T>::type f(T val)
{
    f2(val);
}
#else
  #error "One of TRY1 - TRY7 must be defined"
#endif


int main()
{
    int i = 5;
    f(i);
    f(&i);

    return 0;
}
