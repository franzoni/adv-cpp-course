// algorithms
#include <iostream>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <numeric>

namespace
{
using std::cout;

template <typename Cont>
void printCont(Cont &c, std::ostream &os = cout)
{
    for (auto &val: c)
    {
        os << val << ' ';
    }
    os << '\n';
}

} // unnamed namespace

int main()
{
    std::vector<int> nums = {8, 3, 98, 17, 3, -3, 18, 3, 8};
    printCont(nums);

    // count, find
    cout << "Number of '3's: "
         << std::count(nums.begin(), nums.end(), 3) << '\n';
    cout << "Even numbers: "
         << std::count_if(nums.begin(), nums.end(),
                     [] (int i) { return (i%2) == 0; })
         << '\n';
    cout << "Position of first negative number: "
         << std::find_if(nums.begin(), nums.end(),
                    [] (int i) { return i < 0; }) - nums.begin()
         << '\n';

    cout << "has negative? " << std::boolalpha
         << std::any_of(nums.begin(), nums.end(),
                        [] (int i) { return i < 0; })
         << '\n';

    // fill, replace, remove
    fill_n(nums.begin(), 4, 0);
    cout << "first 4 filled with 0: " << '\n';
    printCont(nums);

    replace(nums.begin(), nums.end(), 0, 99);
    cout << "0 replaced by 99: " << '\n';
    printCont(nums);

    auto newEnd = remove(nums.begin(), nums.end(), 99);
    cout << "99's removed, before erase: " << '\n';
    printCont(nums);

    nums.erase(newEnd, nums.end());
    cout << "99's removed, after erase: " << '\n';
    printCont(nums);

    return 0;
}
