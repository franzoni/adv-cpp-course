#include <cstdint>
#include <mutex>
#include <functional>

#include <deque>
#include <numeric>
#include <iostream>

class Account
{
public:
    Account(int64_t initVal)
      : val{initVal}
    {}

    void withdraw(int64_t amnt)
    {
        transaction([=] { val -= amnt; });
    }
    void deposit(int64_t amnt)
    {
        transaction([=] { val += amnt; });
    }
    int64_t balance()
    {
        int64_t v;
        transaction([this, &v] { v = val; });
        return v;
    }

    void transferOut(Account &to, int64_t amnt)
    {
        std::lock_guard<std::mutex> lMe{mtx};
        std::lock_guard<std::mutex> lTo{to.mtx};
        val -= amnt;
        to.val += amnt;
    }

private:

    void transaction(std::function<void()> f)
    {
        std::lock_guard<std::mutex> l{mtx};
        f();
    }

    int64_t val;
    std::mutex mtx;
};

void transfer(Account &from, Account &to, int64_t amnt)
{
    from.transferOut(to, amnt);
}

void printAl(std::deque<Account> &al)
{
    for (size_t i{0}; i != al.size(); ++i)
    {
        std::cout << "a[" << i << "]: " << al[i].balance() << '\n';
    }
}

int main()
{
    std::deque<Account> acLst;
    acLst.emplace_back(2000);
    acLst.emplace_back(7500);
    acLst.emplace_back(2300);
    acLst.emplace_back(1200);
    printAl(acLst);

    transfer(acLst[1], acLst[2], 1500);
    printAl(acLst);

    auto sum{std::accumulate(acLst.begin(), acLst.end(), 0LL,
                             [] (int64_t s, Account &a)
                             {
                                 return s + a.balance();
                             })};
    std::cout << "sum: " << sum << '\n';

    return 0;
}
