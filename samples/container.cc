// testing container element requirements

#include <iostream>
#include <list>
#include <deque>
#include <vector>
#include <array>
#include <type_traits>
#include "printHelper.hh"

using std::cout;

// X is
//   - not assignable
//   - not copy/move constructible
//   - not default constructible
class X
{
public:
    X(int i) : val{i} {}
    X(X &&) = delete;

    int get() const { return val; }

private:
    int val;
};
static_assert(!std::is_default_constructible_v<X>);
static_assert(!std::is_move_constructible_v<X>);
static_assert(!std::is_move_assignable_v<X>);
static_assert(!std::is_copy_constructible_v<X>);
static_assert(!std::is_copy_assignable_v<X>);

// Y is
//   - not assignable
//   - move constructible
//   - not default constructible
class Y
{
public:
    Y(int i) : val{i} {}
    Y(Y &&) = default;

    int get() const { return val; }

private:
    int val;
};
static_assert(!std::is_default_constructible_v<Y>);
static_assert(std::is_move_constructible_v<Y>);
static_assert(!std::is_move_assignable_v<Y>);
static_assert(!std::is_copy_constructible_v<Y>);
static_assert(!std::is_copy_assignable_v<Y>);

// Z is
//   - move assignable
//   - move constructible
//   - not default constructible
class Z
{
public:
    Z(int i) : val{i} {}
    Z(Z &&) = default;
    Z &operator=(Z &&) = default;

    int get() const { return val; }

private:
    int val;
};
static_assert(!std::is_default_constructible_v<Z>);
static_assert(std::is_move_constructible_v<Z>);
static_assert(std::is_move_assignable_v<Z>);
static_assert(!std::is_copy_constructible_v<Z>);
static_assert(!std::is_copy_assignable_v<Z>);


// tesing container construction, erase
template <template<typename> class C, typename T>
C<T> testBase()
{
    std::array arr{1, 2, 3, 5};
    C<T> c{arr.begin(), arr.end()};

    // we have guaranteed copy elision, so we can return c even if not copyable
    return c;
}

template <template<typename, typename> class C, typename T, typename A>
void testBack(C<T, A> &c)
{
    c.emplace_back(7);
    c.pop_back();
}

template <template<typename, typename> class C, typename T, typename A>
void testMiddle(C<T, A> &c)
{
    // we have guaranteed copy-elision, but it doesn't apply here
    // c.insert(c.end(), 7); // error: copy constructor deleted
    auto it = c.emplace(++(c.begin()), 9);
    c.erase(it);

}


// we need guaranteed single parameter templates
// and alias templates are not allowed in function scope
template <typename T> using MyList = std::list<T>;
template <typename T> using MyDeque = std::deque<T>;
template <typename T> using MyVector = std::vector<T>;

int main()
{
    auto c1 = testBase<MyList, X>();
    testBack(c1);
    testMiddle(c1);
    printOut(c1, [] (X const &elem) { return elem.get(); });

    auto c2 = testBase<MyDeque, X>();
    testBack(c2);
    //testMiddle(c2);  // fails, emplace requires move (assignment and ctor)
                       // for deque and vector
    printOut(c2, [] (X const &elem) { return elem.get(); });

    auto c3 = testBase<MyVector, X>();
    //testBack(c3);  // fails, vector::emplace_back requires move ctor
    printOut(c3, [] (X const &elem) { return elem.get(); });

    auto c4 = testBase<MyList, Y>();
    testBack(c4);
    testMiddle(c4);
    printOut(c4, [] (Y const &elem) { return elem.get(); });

    auto c5 = testBase<MyDeque, Y>();
    testBack(c5);
    //testMiddle(c5);  // fails, emplace requires move (assignment and ctor)
                       // for deque and vector
    printOut(c5, [] (Y const &elem) { return elem.get(); });

    auto c6 = testBase<MyVector, Y>();
    testBack(c6);  // ok, vector::emplace_back only requires move ctor
    printOut(c6, [] (Y const &elem) { return elem.get(); });

    auto c7 = testBase<MyList, Z>();
    testBack(c7);
    testMiddle(c7);
    printOut(c7, [] (Z const &elem) { return elem.get(); });

    auto c8 = testBase<MyDeque, Z>();
    testBack(c8);
    testMiddle(c8);
    printOut(c8, [] (Z const &elem) { return elem.get(); });

    auto c9 = testBase<MyVector, Z>();
    testBack(c9);
    testMiddle(c9);
    printOut(c9, [] (Z const &elem) { return elem.get(); });

    return 0;
}


