/*
 * Copyright (c) 2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <optional>
#include <iostream>

std::optional<int> optdiv(int a, int b)
{
    if (b != 0)
    {
        return {a/b};
    }
    else
    {
        return {};
    }
}


int main()
{
    auto result = optdiv(33, 5);
    if (result)
    {
        std::cout << *result << '\n';
    }
    else
    {
        std::cout << "No result\n";
    }

    return 0;
}
