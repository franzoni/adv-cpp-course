#ifndef _EXPERIMENTAL_N3554_POLICY_PARALLEL
#error Include <parallel/algorithm> or <parallel/utility>
#endif

#include <algorithm>
#include <experimental/bits/parallel/algos/par/diffract.h>

namespace std {
namespace experimental {
namespace parallel {
inline namespace v1 {

  namespace detail {
    template<class InputIterator, class T, class UnaryOperation, class BinaryOperation>
    T partial_transform_reduce(InputIterator first, InputIterator last, UnaryOperation convOp, BinaryOperation reduceOp)
    {
      //typedef typename std::iterator_traits<InputIterator>::value_type InT;
      if (first == last) return convOp(*first);
      T init{convOp(*first)};
      while (++first != last)
      {
          init = forward<T>(reduceOp(forward<T>(init), convOp(*first)));
      }
      return init;
    }
  } 

  template<class InputIterator, class UnaryOperation, class T, class BinaryOperation>
    typename remove_reference<T>::type
      parallel_execution_policy::transform_reduce(InputIterator first, InputIterator last,
                                                    UnaryOperation convOp,
                                                    T &&init,
                                                    BinaryOperation reduceOp)
  {
    typedef typename remove_reference<T>::type Tv;
    if(first == last) return forward<Tv>(init);
    return reduceOp(forward<Tv>(init),
                    forward<Tv>(detail::diffract_gather(first, last,
                                                       detail::partial_transform_reduce<InputIterator, Tv, UnaryOperation, BinaryOperation>,
                                                       reduceOp,
                                                       convOp, reduceOp)));
  }
/*
  template<class InputIterator, class T>
    T parallel_execution_policy::reduce(InputIterator first, InputIterator last, T init) const 
  {
    return this->reduce(first, last, init, ::std::plus<T>());
  }

  template<class InputIterator>
    typename iterator_traits<InputIterator>::value_type
      parallel_execution_policy::reduce(InputIterator first, InputIterator last) const {
    typedef typename iterator_traits<InputIterator>::value_type T;
    return this->reduce(first, last, T{});
  }
*/
} // namespace v1
} // namespace parallel
} // namespace experimental
} // namespace std
