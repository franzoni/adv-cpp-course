#ifndef _EXPERIMENTAL_N3554_POLICY_SEQUENTIAL
#error Include <parallel/algorithm> or <parallel/utility>
#endif

#include <algorithm>
#include <numeric>

namespace std {
namespace experimental {
namespace parallel {
inline namespace v1 {
#if 0
  template<class InputIterator>
    typename iterator_traits<InputIterator>::value_type
      sequential_execution_policy::transform_reduce(InputIterator first, InputIterator last) const {
    typedef typename iterator_traits<InputIterator>::value_type T;
    return std::accumulate(first, last, T{});
  }

  template<class InputIterator, class T>
    T sequential_execution_policy::reduce(InputIterator first, InputIterator last, T init) const {
    return std::accumulate(first, last, init);
  }
#endif
  template<class InputIterator, class UnaryOperation, class T, class BinaryOperation>
    T &&sequential_execution_policy::transform_reduce(InputIterator first, InputIterator last,
                                                    UnaryOperation convOp,
                                                    T &&init,
                                                    BinaryOperation reduceOp)
  {
    //typedef typename std::iterator_traits<InputIterator>::value_type InT;
    for (auto i = first; i != last; ++i)
    {
      //init = std::forward<T>(binary_op(std::forward<T>(init), unary_op(std::forward<value_type>(*i))));
      init = std::forward<T>(reduceOp(forward<T>(init), convOp(*i)));
    }
    return std::forward<T>(init);
    //return std::accumulate(first, last, init, binary_op);
  }

} // namespace v1
} // namespace parallel
} // namespace experimental
} // namespace std
