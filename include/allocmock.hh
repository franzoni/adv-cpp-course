/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef ALLOCMOCK_HH_SEEN_
#define ALLOCMOCK_HH_SEEN_
#include "trompeloeil.hpp"
#include <cstddef>
#include <cstdlib>
#include <array>

//#define DEBUG 1
#ifdef DEBUG
// hardcore debugging
void writeLog(int line);
struct HCLogger
{
    HCLogger(int line)
    {
        writeLog(line);
    }
};
#define LOGFILE(line) "dbg." ## #line
#define VARNAME(line) dbgVar ## line
//#define HCLOG HCLogger VARNAME(__LINE__)(LOGFILE(__LINE__))
#define HCLOG HCLogger VARNAME(__LINE__)(__LINE__)

#define USE_LOG 1
#include "log-helper.hh"
#endif // DEBUG

namespace exerciseTest
{
class AllocTracer
{
public:
    virtual ~AllocTracer() = default;

    virtual void trackNew(void *, size_t) = 0;
    virtual void trackDelete(void *) = 0;
};

template <typename T, size_t N>
class FixedVect
{
public:
    void push_back(T elem)
    {
        if (curSz == N)
        {
            throw std::bad_alloc{};
        }
        store[curSz++] = elem;
    }
    void pop_back()
    {
        --curSz;
    }
    T back() const
    {
        return store[curSz-1];
    }
    size_t size() const
    {
        return curSz;
    }

private:
    std::array<T, N> store;
    size_t curSz = 0;
};

template <size_t N>
class PointerTracker
{
public:
    void insert(void *p)
    {
        auto found = std::find(store.begin(), store.end(), nullptr);
        if (found != store.end())
        {
            *found = p;
        } else {
            throw std::bad_alloc{};
        }
    }

    bool checkAndRemove(void *p)
    {
        auto found = std::find(store.begin(), store.end(), p);
        if (found != store.end())
        {
            *found = nullptr;
            return true;
        }
        return false;
    }

private:
    std::array<void *, N> store {nullptr};
};

class AllocMock
{
public:
    AllocMock(AllocTracer *t = nullptr)
      : pointers{}
      , tracer{t}
      , count{0}
      , good{true}
    {
        allocs.push_back(this);
    }
    ~AllocMock()
    {
        allocs.pop_back();
    }

    void *alloc(size_t n)
    {
        void *p{malloc(n)};
        // avoid recursion by allocs caused by tracer
        auto oldTracer = tracer;
        tracer = nullptr;
        if (oldTracer)
        {
            pointers.insert(p);
            oldTracer->trackNew(p, n);
        }
        tracer = oldTracer;
        ++count;
        return p;
    }
    void dealloc(void *p)
    {
        if (!p) return;
        auto oldTracer = tracer;
        tracer = nullptr;
        if (oldTracer)
        {
            oldTracer->trackDelete(p);
            if (!pointers.checkAndRemove(p))
            {
                good = false;
            }
        }
        tracer = oldTracer;
        --count;
        free(p);
    }

    bool ok() const
    {
        return good;
    }

    int getCount() const
    {
        return count;
    }

    void setTracer(AllocTracer *t)
    {
        tracer = t;
    }

    static AllocMock *getCurrent()
    {
        if (allocs.size() > 0)
        {
            return allocs.back();
        } else {
            return nullptr;
        }
    }

private:
    static FixedVect<AllocMock *, 10> allocs;

    PointerTracker<50> pointers;
    AllocTracer *tracer;
    int count;
    bool good;
};

struct AllocTracerMock : public AllocTracer
{
    MAKE_MOCK2(trackNew, void(void *, size_t));
    MAKE_MOCK1(trackDelete, void(void *));
};

struct TraceSetter
{
    TraceSetter(AllocMock &a, AllocTracer *t)
      : alloc(&a)
    {
        a.setTracer(t);
    }
    ~TraceSetter()
    {
        alloc->setTracer(nullptr);
    }

    AllocMock *alloc;
};
struct TraceDeleter
{
    explicit TraceDeleter(AllocMock &a)
      : alloc(&a)
    {
    }
    ~TraceDeleter()
    {
        alloc->setTracer(nullptr);
    }

    AllocMock *alloc;
};
} // namespace exerciseTest
#endif /* ALLOCMOCK_HH_SEEN_ */
