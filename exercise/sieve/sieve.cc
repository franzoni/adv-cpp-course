// sieve.cc - single-threaded version
/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <iostream>
#include <bitset>
#include <cstdlib>
#include <cstdint>

#include "timeHelper.hh"

namespace exercise
{
using std::cout;
using std::bitset;


constexpr uint64_t maxRangeSize = 10'000'000;
typedef bitset<maxRangeSize> SetT;

constexpr unsigned defSlicesCnt = 2;
constexpr unsigned maxSlicesCnt = 16;
constexpr uint64_t maxPrime = maxRangeSize * defSlicesCnt;

SetT markSet[maxSlicesCnt];


void computeRange(uint64_t lower, uint64_t rangeSize, SetT &marked)
{
    uint64_t curPrime = 2, start = lower;
    uint64_t upper = lower + rangeSize;

    if (lower < 2)
    {
	if (lower == 0)
	{
	    marked[1] = true;
	}
	marked[0] = true;
	start = 2;
    }

    while (curPrime * curPrime < upper)
    {
	uint64_t curVal = start / curPrime;
	if (curVal < 2)
	{
	    curVal = 2;
	}
	curVal = curPrime * curVal;
	if (curVal < start)
	{
	    curVal += curPrime;
	}

	for (uint64_t i = curVal - lower;
	     curVal < upper;
	     i += curPrime, curVal += curPrime)
	{
	    marked[i] = true;
	}

	curPrime += 1;
    }
}
} // namespace exercise

int main(int argc, char **argv)
{
    using namespace exercise;

    // compute partitioning
    unsigned long slicesCnt = defSlicesCnt;
    if (argc == 2)
    {
        slicesCnt = strtoul(argv[1], nullptr, 0);
        if (slicesCnt == 0) slicesCnt = defSlicesCnt;
        if (slicesCnt > maxSlicesCnt) slicesCnt = maxSlicesCnt;
    }
    uint64_t sliceSize = maxPrime / slicesCnt;
    uint64_t sliceStart = 0;
    cout << "sieving " << maxPrime << " numbers in " << slicesCnt << " slices\n";

    // compute the partitions
    Timer tAll;

    for (unsigned i = 0; i != slicesCnt; ++i, sliceStart += sliceSize)
    {
        cout << "starting slice " << i;
        cout << ", start " << sliceStart;
        cout << ", slice size: " << sliceSize << '\n';

        Timer ts;
        computeRange(sliceStart, sliceSize, markSet[i]);

        cout << "elapsed microseconds for slice " << i
             << ": " << ts << '\n';
    }

    cout << "total elapsed microseconds: " << tAll << '\n';

    // find last computed prime to check for correctness
    sliceStart = 0;
    uint64_t pCount = 0;
    uint64_t lastPrime = 0;
    for (unsigned i = 0; i != slicesCnt; ++i, sliceStart += sliceSize)
    {
        for (uint64_t j = 0, val = sliceStart; j != sliceSize; ++j, ++val)
        {
            if (!markSet[i][j])
            {
                ++pCount;
                lastPrime = val;
            }
        }
    }

    cout << "found " << pCount << " primes:" << '\n';
    for (unsigned i = 0, cnt = 0; cnt != 20; ++i)
    {
        if (!markSet[0][i])
        {
            cout << i << ", ";
            ++cnt;
        }
    }
    cout << "\nlast prime found: " << lastPrime << '\n';

    return 0;
}
