Exercise "Synchronization"
base: sync

This is a sample class that uses lazy evaluation.
If accessed from different threads, this causes data races
(thread sanitizer tells you about them).

 - Fix the data races with a mutex.

 - Fix the data races with call_once()
