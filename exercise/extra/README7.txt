Exercise "Dynamic Memory w/ Allocator"
base stdlib

 - provide an allocator as derivation of std::pmr::memory_resource
   that only allows one allocation and throws on a second allocation
   (internally probably just uses new/delete or malloc/free)

 - define StaticVector as derivation of std::pmr::vector

 - redefine StaticVector ctors
   (we only need one with size and one with initializer_list)
   and do an initial reserve()
