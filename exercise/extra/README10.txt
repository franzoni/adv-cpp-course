Exercise "Class perfect forwarding"

 - Text has a function setText(std::string const &txt).
   Add a version with move semantics.
 - Modify calls in demo.cc that also test the move version.
 - Combine both functions into one using perfect forwarding.
