Exercise "Template specialization"

 - Make 'fill' a template parameter of PathShape.
   Separate 'preparePath' from the fill logic
   in a helper function template finalDraw.
   Provide respective specializations.
   
 - Again, adjust calls in demo.cc accordingly.
 - Separate implementation out of header.
   Use explicit instantiation to make the required instances available.
