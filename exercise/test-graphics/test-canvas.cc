/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include "cairomock.hh"

#include "lest.hpp"
#include "trompeloeil.hpp"
#include "allocmock.hh"
#include "test-classes.hh"
#include "xwin-mock.hh" // this must be before including canvas.hh

#include <canvas.hh>

#include <type_traits>
#include <array>

namespace
{
using namespace exercise;
using namespace exerciseTest;

using trompeloeil::_;

static_assert(!std::is_copy_constructible<Canvas>::value, "Canvas must not be copy constructible");
static_assert(!std::is_copy_assignable<Canvas>::value, "Canvas must not be copy assignable");

#ifdef HAS_CANVAS_MOVE_CTOR
static_assert(std::is_move_constructible<Canvas>::value, "Canvas must be move constructible");
#endif
#ifdef HAS_CANVAS_MOVE_ASSIGNMENT
static_assert(std::is_move_assignable<Canvas>::value, "Canvas must be move assignable");
#endif

std::string longString = "A pretty long string that should be long enough to not fit into any small string optimization";

//XWinMock *oldXMock = nullptr;  // @@@should be obsolete

std::vector<Canvas *> testObjs;

void testSetup()
{
    testObjs.reserve(50);
}

[[maybe_unused]]
Canvas *newTestObj(int w = 0, int h = 0, std::string name = std::string())
{
    auto c = new Canvas{w, h, name};
    testObjs.push_back(c);
    return c;
}

#ifdef HAS_CANVAS_MOVE_CTOR
Canvas *newTestObj(Canvas &&rhs)
{
    auto c = new Canvas{std::move(rhs)};
    testObjs.push_back(c);
    return c;
}
#endif

void resetTestObjs()
{
    XWinWrap::helperPtr = nullptr;
    XWinWrap::cairoWrapPtr = nullptr;
    globalGuiSeq = nullptr;
    globalForbidden = nullptr;
}

extern const lest::test canvasTests[] =
{
    {CASE("canvas: create and destroy functions match")
     {
         XWinTestHelper winHelper;
         XWinWrap::helperPtr = &winHelper;
         std::array<GuiFuncs, 12> seqBuf{GuiFuncs(0)};
         Sequencer<GuiFuncs> seq(seqBuf.data(), seqBuf.size());
         winHelper.seqObj = &seq;

         CairoWrap testCairo;
         testCairo.seq = &seq;
         XWinWrap::cairoWrapPtr = &testCairo;

         TestShape *s = new TestShape;
         s->drawFuncMarker = &seq;

         std::array<GuiFuncs, 12> funcSequence{winCreate
                                               , winSurface
                                               , cairoCreate
                                               , registerCb};
         {
             Canvas c{1, 44, "tc"};
             EXPECT(seqBuf == funcSequence);

             EXPECT(winHelper.cb != nullptr);

             c += s;
             winHelper.cb();

             funcSequence[4] = shapeDraw;
             EXPECT(seqBuf == funcSequence);
         } // now Canvas c is destructed
         EXPECT(winHelper.cb == nullptr);
         funcSequence[5] = unregisterCb;
         funcSequence[6] = cairoDestroy;
         funcSequence[7] = surfaceDestroy;
         funcSequence[8] = winClean;
         EXPECT(seqBuf == funcSequence);

         resetTestObjs();
     }},

    {CASE("Canvas: check shape release in dtor")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc{};

         //trompeloeil::stream_tracer log(std::cout);

         {
             TestShape *s1{nullptr};
             TestShape *s2{nullptr};

             int cnt1{0};

/*
 * Intercepting new/delete is a bit complicated...
 * ALLOW_CALL creates objects using new (and releases them with delete).
 * trackNew() is only provided in tracer after the expectation is
 * setup with ALLOW_CALL.
 * So we can't use the tracer before trackNew() is setup.
 * As we setup the tracer only after setting up trackNew(), these new()-
 * calls are not counted.
 */
             {
                 ALLOW_CALL(tracer, trackDelete(_))
                     .LR_SIDE_EFFECT(--cnt1);

                 ALLOW_CALL(tracer, trackNew(_, _))
                     .LR_SIDE_EFFECT(++cnt1);

                 TraceSetter ts{testAlloc, &tracer};

                 s1 = new TestShape{"Hello"};
                 s2 = new TestShape{longString};
             }
             EXPECT(testOk());
             int cnt2{0};
             {
                 ALLOW_CALL(tracer, trackDelete(_))
                     .LR_SIDE_EFFECT(--cnt2);

                 ALLOW_CALL(tracer, trackNew(_, _))
                     .LR_SIDE_EFFECT(++cnt2);

                 TraceSetter ts{testAlloc, &tracer};

                 Canvas c(1, 44, "tc");

                 c += s1;
                 c += s2;
             }

             EXPECT(testOk());
             EXPECT(cnt2 == -cnt1);
         }

         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);

         resetTestObjs();
     }},

    {CASE("Canvas: check drawing order")
     {
         //trompeloeil::stream_tracer log(std::cout);
         Canvas c{2, 3, "c"};
         std::array<int, 10> seqBuf{0};
         Sequencer<int> seq(seqBuf.data(), seqBuf.size());
         c += new TestShape{1, &seq};
         c += new TestShape{2, &seq};
         c += new TestShape{3, &seq};
         c += new TestShape{4, &seq};
         c += new TestShape{5, &seq};

         c.show();

         std::array<int, 10> canvasDrawSequence{1, 2, 3, 4, 5, 0, 0, 0, 0, 0};
         EXPECT(seqBuf == canvasDrawSequence);
         EXPECT(testOk());

         resetTestObjs();
     }},

    {CASE("Canvas: check correctly registered callback")
     {
         XWinTestHelper winHelper;
         XWinWrap::helperPtr = &winHelper;

         Canvas c{2, 3, "c"};
         std::array<int, 10> seqBuf{0};
         Sequencer<int> seq{seqBuf.data(), seqBuf.size()};
         c += new TestShape{1, &seq};
         c += new TestShape{2, &seq};
         c += new TestShape{3, &seq};
         c += new TestShape{4, &seq};
         c += new TestShape{5, &seq};

         winHelper.cb();

         std::array<int, 10> canvasDrawSequence{1, 2, 3, 4, 5, 0, 0, 0, 0, 0};
         EXPECT(seqBuf == canvasDrawSequence);

         resetTestObjs();
     }},

#ifdef HAS_CANVAS_MOVE_CTOR
#ifndef HAS_CANVAS_PIMPL
    // all function calls on a moved-from pImpl fail for nullptr access
    {CASE("Canvas move ctor must move (not copy) elements (moved-from must be empty)")
     {
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{seqBuf.data(), seqBuf.size()};
         Canvas *movedFrom = newTestObj();
         *movedFrom += new TestShape{1, &seq};
         *movedFrom += new TestShape{2, &seq};

         Canvas movedTo{std::move(*movedFrom)};

         // old elements should be empty, so draw() does nothing
         std::array<int, 5> oldDrawSequence{0, 0, 0, 0, 0};
         movedFrom->draw();
         EXPECT(seqBuf == oldDrawSequence);

         // new elements should have shapes now, so draw() draws elements
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         movedTo.draw();
         EXPECT(seqBuf == newDrawSequence);

         resetTestObjs();
     }},
#endif

    {CASE("Canvas destructor of moved-from after move ctor must not clean up")
     {
         XWinTestHelper winHelper;
         XWinWrap::helperPtr = &winHelper;
         Canvas *movedFrom = newTestObj();
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{seqBuf.data(), seqBuf.size()};
         *movedFrom += new TestShape{1, &seq};
         *movedFrom += new TestShape{2, &seq};

         XWinWrap::helperPtr = nullptr;

         Canvas *movedTo = newTestObj(std::move(*movedFrom));

         // for destruction we can't work just with a sequence
         // problem is unregisterCallback called on a nullptr
         // so we need to forbid such calls and avoid continuation,
         // i.e. we need to throw an exception
         // using tromploeil would probably be the easiest option...
         ForbiddenCall noCalls{unregisterCb
                               , cairoDestroy
                               , surfaceDestroy
                               , winClean
                               , shapeClean};
         globalForbidden = &noCalls;

         // we use an additional sequence to get a positive feedback
         std::array<GuiFuncs, 5> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> fSeq(functionSeq.data(), functionSeq.size());
         globalGuiSeq = &fSeq;

         std::array<GuiFuncs, 5> emptySeq{GuiFuncs(0)};
         EXPECT(functionSeq == emptySeq);

         try
         {
             movedFrom->~Canvas();
         }
         catch(Forbidden &e)
         {
             e.print(std::cerr);
         }

         EXPECT(noCalls.forbiddenFunctionCalled == false);
         EXPECT(functionSeq == emptySeq);

         // the moved Canvas must still draw the elements
         movedTo->draw();
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         EXPECT(seqBuf == newDrawSequence);

         resetTestObjs();

         // restore movedFrom to a destructible state
         new (movedFrom) Canvas(0, 0, std::string());
     }},

    {CASE("Canvas move ctor register/unregister must match")
     {
         XWinTestHelper winHelper;
         XWinWrap::helperPtr = &winHelper;
         Canvas *movedFrom = newTestObj();

         XWinWrap::helperPtr = nullptr;

         EXPECT(winHelper.wrongRegisterCbOnFull == false);
         EXPECT(winHelper.wrongUnregCbOnEmpty == false);

         // move
         Canvas movedTo{std::move(*movedFrom)};
         EXPECT(winHelper.wrongRegisterCbOnFull == false);
         EXPECT(winHelper.wrongUnregCbOnEmpty == false);
         EXPECT(bool(winHelper.cb) == true);

         resetTestObjs();
     }},

    {CASE("Canvas move ctor must move callback (unregister/register)")
     {
         // we can't really test for unregister/register,
         // as we may have CanvasImpl
         // to detect CanvasImpl or not we would need to
         // track our vector
         // and even this would not really help as we might have
         // vector<unique_ptr>
         // However, to have this helpful test anyway,
         // we add a Makefile #define for this
         // and we assume here that XWin is hold via pointer, not by value
         // so the XWin object stays the same on Canvas move
         XWinTestHelper winHelper;
         XWinWrap::helperPtr = &winHelper;
         Canvas *movedFrom = newTestObj();
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{seqBuf.data(), seqBuf.size()};
         *movedFrom += new TestShape{1, &seq};
         *movedFrom += new TestShape{2, &seq};

         std::array<GuiFuncs, 12> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> functSeq(functionSeq.data(), functionSeq.size());
         globalGuiSeq = &functSeq;

         XWinWrap::helperPtr = nullptr;

         Canvas movedTo{std::move(*movedFrom)};

#ifndef HAS_CANVAS_PIMPL
         EXPECT(functionSeq[0] == unregisterCb);
         EXPECT(functionSeq[1] == registerCb);
#endif

         // call moved callback
         winHelper.cb();
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         EXPECT(seqBuf == newDrawSequence);

         resetTestObjs();
     }},

    {CASE("Canvas destructor of moved-to after move ctor must properly clean up")
     {
         Canvas *movedFrom = newTestObj();
         *movedFrom += new TestShape;

         Canvas *movedTo = newTestObj(std::move(*movedFrom));

         std::array<GuiFuncs, 12> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> fSeq(functionSeq.data(), functionSeq.size());
         globalGuiSeq = &fSeq;

         movedTo->~Canvas();

         std::array<GuiFuncs, 12> funcSequence{unregisterCb
                                               , shapeClean
                                               , cairoDestroy
                                               , surfaceDestroy
                                               , winClean};
         EXPECT(functionSeq == funcSequence);

         resetTestObjs();

         // restore movedTo to a destructible state
         new (movedTo) Canvas(0, 0, std::string());
     }},

    {CASE("Check full Canvas move ctor sequence")
     {
         EXPECT(true);
         //trompeloeil::stream_tracer log(std::cout);
         unsigned char cbuf1[sizeof(Canvas)];
         Canvas *c1{static_cast<Canvas *>(static_cast<void *>(cbuf1))};

         unsigned char cbuf2[sizeof(Canvas)];
         Canvas *c2{static_cast<Canvas *>(static_cast<void *>(cbuf2))};

         GuiWinMock testWin;
         GuiWinWrap const * win1{nullptr};
         GuiWinWrap const * win2{nullptr};

         CairoMock testCairo;
         CairoMock *cp = &testCairo;

         auto testShape = new ShapeMock;

         trompeloeil::sequence seq;
         std::function<void()> cb1{nullptr};
         std::function<void()> cb2{nullptr};

         {
             REQUIRE_CALL(testWin, create(_))
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, getSurface())
                 .RETURN(cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testCairo, cairo_create(_))
                 .WITH(_1 == cp)
                 .RETURN(cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_SIDE_EFFECT(win1 = _1)
                 .LR_SIDE_EFFECT(cb1 = _2)
                 .IN_SEQUENCE(seq);

             new (c1) Canvas{77, -77, "C"};
             *c1 += testShape;
         }
         EXPECT(testOk());
         EXPECT(cb1 != nullptr);

         {
             FORBID_CALL(testWin, create(_));
             FORBID_CALL(testWin, getSurface());
             FORBID_CALL(testCairo, cairo_create(_));

             ALLOW_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win1)
                 .LR_SIDE_EFFECT(cb1 = nullptr)
                 .IN_SEQUENCE(seq);
             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_WITH(cb1 == nullptr)
                 .LR_SIDE_EFFECT(win2 = _1)
                 .LR_SIDE_EFFECT(cb2 = _2)
                 .IN_SEQUENCE(seq);
             ALLOW_CALL(*testShape, doDraw(_));

             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             new (c2) Canvas{std::move(*c1)};
             if (cb1) // we may have pimpl
             {
                 cb2 = cb1;
                 cb1 = nullptr;
                 win2 = win1;
             }
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape, doDraw(_))
                 .WITH(_1 == cp);

             cb2();
         }
         EXPECT(testOk());

         {
             FORBID_CALL(testWin, unregisterCb(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             c1->~Canvas();
         }
         EXPECT(true);
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape, doDraw(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             cb2();
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win2)
                 .LR_SIDE_EFFECT(cb2 = nullptr;)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testCairo, cairo_destroy(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testCairo, cairo_surface_destroy(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, clean(_))
                 .IN_SEQUENCE(seq);

             c2->~Canvas();
         }
         EXPECT(testOk());
     }},

    // this is optional: we don't really require a moved-from
    // object to be able to move again
    {CASE("Moved-from Canvas moved again by ctor should not do anything")
     {
         XWinTestHelper winHelper;
         XWinWrap::helperPtr = &winHelper;
         Canvas *movedFrom = newTestObj();
         *movedFrom += new TestShape;

         XWinWrap::helperPtr = nullptr;

         Canvas *movedTo1 = newTestObj();
         movedTo1->~Canvas();
         // here we move movedFrom the first time
         new (movedTo1) Canvas{std::move(*movedFrom)};

         Canvas *movedTo2 = newTestObj();
         movedTo2->~Canvas();

         ForbiddenCall noCalls{winCreate
                               , winSurface
                               , cairoCreate
                               , registerCb
                               , unregisterCb
                               , cairoDestroy
                               , surfaceDestroy
                               , winClean
                               , shapeClean};
         globalForbidden = &noCalls;

         // we use an additional sequence to get a positive feedback
         std::array<GuiFuncs, 10> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> fSeq(functionSeq.data(), functionSeq.size());
         globalGuiSeq = &fSeq;

         std::array<GuiFuncs, 10> emptySeq{GuiFuncs(0)};
         EXPECT(functionSeq == emptySeq);

         // here we move the now empty movedFrom again
         try
         {
             new (movedTo2) Canvas{std::move(*movedFrom)};
         }
         catch(Forbidden &e)
         {
             e.print(std::cerr);
         }

         EXPECT(noCalls.forbiddenFunctionCalled == false);
         EXPECT(functionSeq == emptySeq);

         // here we destruct the empty movedTo
         try
         {
             movedTo2->~Canvas();
         }
         catch(Forbidden &e)
         {
             e.print(std::cerr);
         }

         EXPECT(noCalls.forbiddenFunctionCalled == false);
         EXPECT(functionSeq == emptySeq);

         resetTestObjs();

         // restore destructed movedTo2 to a destructible state
         new (movedTo2) Canvas(0, 0, std::string());
     }},

    {CASE("Check correct moved-from ctor handling completely for Canvas")
     {
         //trompeloeil::stream_tracer log(std::cout);
         unsigned char cbuf1[sizeof(Canvas)];
         Canvas *c1{static_cast<Canvas *>(static_cast<void *>(cbuf1))};

         unsigned char cbuf2[sizeof(Canvas)];
         Canvas *c2{static_cast<Canvas *>(static_cast<void *>(cbuf2))};

         unsigned char cbuf3[sizeof(Canvas)];
         Canvas *c3{static_cast<Canvas *>(static_cast<void *>(cbuf3))};

         GuiWinMock testWin;
         auto testShape = new ShapeMock;
         GuiWinWrap const * win1{nullptr};
         GuiWinWrap const * win2{nullptr};

         std::function<void()> cb1{nullptr};
         std::function<void()> cb2{nullptr};

         CairoMock testCairo;
         CairoMock *cp = &testCairo;

         // setup and allow everything (already tested before)
         {
             ALLOW_CALL(testWin, create(_));
             ALLOW_CALL(testWin, getSurface())
                 .RETURN(cp);
             ALLOW_CALL(testCairo, cairo_create(_))
                 .RETURN(cp);
             REQUIRE_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_SIDE_EFFECT(win1 = _1)
                 .LR_SIDE_EFFECT(cb1 = _2);

             new (c1) Canvas{77, -77, "C"};
             *c1 += testShape;
         }
         EXPECT(testOk());

         {
             ALLOW_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win1)
                 .LR_SIDE_EFFECT(cb1 = nullptr);
             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_WITH(cb1 == nullptr)
                 .LR_SIDE_EFFECT(win2 = _1)
                 .LR_SIDE_EFFECT(cb2 = _2);

             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             new (c2) Canvas{std::move(*c1)};
             if (cb1) // we may have pimpl
             {
                 cb2 = cb1;
                 cb1 = nullptr;
                 win2 = win1;
             }
         }
         EXPECT(testOk());

         // move moved-from again
         // this possibly just crashes in Canvas ctor if handled wrongly
         {
             FORBID_CALL(testWin, registerCb(_, ANY(std::function<void()>)));
             FORBID_CALL(testWin, unregisterCb(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             new (c3) Canvas{std::move(*c1)};
         }
         EXPECT(testOk());

         {
             FORBID_CALL(testWin, registerCb(_, ANY(std::function<void()>)));
             FORBID_CALL(testWin, unregisterCb(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             c3->~Canvas();
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape, doDraw(_))
                 .WITH(_1 == cp);

             cb2();
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win2);
             REQUIRE_CALL(testCairo, cairo_destroy(_));
             REQUIRE_CALL(testCairo, cairo_surface_destroy(_));
             REQUIRE_CALL(testWin, clean(_));

             c2->~Canvas();
         }
         EXPECT(testOk());

         {
             FORBID_CALL(testWin, registerCb(_, ANY(std::function<void()>)));
             FORBID_CALL(testWin, unregisterCb(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             c1->~Canvas();
         }
         EXPECT(testOk());
     }},

    // memory calls are ok (for std::function in registerCallback)
    // but they must add up to zero
    {CASE("Canvas move ctor must not cause any memory overhead")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management

             Canvas c1{2, 55, "tc1"};
             // we don't have a default ctor for Canvas
             Canvas c2{7, 13, "tc2"};
             c2.~Canvas();

             auto memCnt = testAlloc.getCount();
             new (&c2) Canvas{std::move(c1)};
             EXPECT(memCnt == testAlloc.getCount());

             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},

    {CASE("Canvas move ctor must move ownership (no destruction of moved from)")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management

             Canvas c1{2, 55, "tc1"};
             // we don't have a default ctor for Canvas
             Canvas c2{7, 13, "tc2"};
             c2.~Canvas();

             auto memCnt = testAlloc.getCount();
             new (&c2) Canvas{std::move(c1)};
             c1.~Canvas();
             new (&c1) Canvas{std::move(c2)};
             EXPECT(memCnt == testAlloc.getCount());

             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},
#endif // HAS_CANVAS_MOVE_CTOR
#ifdef HAS_CANVAS_MOVE_ASSIGNMENT
#ifndef HAS_CANVAS_PIMPL
    {CASE("Canvas move assignment must move (not copy) elements (moved-from must be empty)")
     {
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{seqBuf.data(), seqBuf.size()};
         Canvas *movedFrom = newTestObj();
         *movedFrom += new TestShape{1, &seq};
         *movedFrom += new TestShape{2, &seq};

         Canvas movedTo{0, 0, std::string{}};

         movedTo = std::move(*movedFrom);

         // old elements should be empty, so draw() does nothing
         std::array<int, 5> oldDrawSequence{0, 0, 0, 0, 0};
         movedFrom->draw();
         EXPECT(seqBuf == oldDrawSequence);

         // new elements should have shapes now, so draw() draws elements
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         movedTo.draw();
         EXPECT(seqBuf == newDrawSequence);

         resetTestObjs();
     }},
#endif

    {CASE("Canvas destructor of moved-from after move assignment must not clean up")
     {
         XWinTestHelper winHelper;
         XWinWrap::helperPtr = &winHelper;
         Canvas *movedFrom = newTestObj();
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{seqBuf.data(), seqBuf.size()};
         *movedFrom += new TestShape{1, &seq};
         *movedFrom += new TestShape{2, &seq};

         XWinWrap::helperPtr = nullptr;

         Canvas *movedTo = newTestObj();

         *movedTo = std::move(*movedFrom);

         ForbiddenCall noCalls{unregisterCb
                               , cairoDestroy
                               , surfaceDestroy
                               , winClean
                               , shapeClean};
         globalForbidden = &noCalls;

         // we use an additional sequence to get a positive feedback
         std::array<GuiFuncs, 5> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> fSeq(functionSeq.data(), functionSeq.size());
         globalGuiSeq = &fSeq;

         std::array<GuiFuncs, 5> emptySeq{GuiFuncs(0)};
         EXPECT(functionSeq == emptySeq);

         try
         {
             movedFrom->~Canvas();
         }
         catch(Forbidden &e)
         {
             e.print(std::cerr);
         }

         EXPECT(noCalls.forbiddenFunctionCalled == false);
         EXPECT(functionSeq == emptySeq);

         // the moved Canvas must still draw the elements
         movedTo->draw();
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         EXPECT(seqBuf == newDrawSequence);

         resetTestObjs();

         // restore movedFrom to a destructible state
         new (movedFrom) Canvas(0, 0, std::string());
     }},

    {CASE("Canvas move assignment register/unregister must match")
     {
         XWinTestHelper winHelper1;
         XWinWrap::helperPtr = &winHelper1;
         Canvas *movedFrom = newTestObj();

         XWinTestHelper winHelper2;
         XWinWrap::helperPtr = &winHelper2;
         Canvas movedTo{-1, -2, std::string()};

         EXPECT(winHelper1.wrongRegisterCbOnFull == false);
         EXPECT(winHelper1.wrongUnregCbOnEmpty == false);

         // move
         movedTo = std::move(*movedFrom);
         // winHelper1 should now be moved to 'movedTo' and still live
         EXPECT(winHelper1.wrongRegisterCbOnFull == false);
         EXPECT(winHelper1.wrongUnregCbOnEmpty == false);
         EXPECT(bool(winHelper1.cb) == true);

         // winHelper2 is overwritten now and should be cleaned up
         EXPECT(winHelper2.wrongRegisterCbOnFull == false);
         EXPECT(winHelper2.wrongUnregCbOnEmpty == false);
         EXPECT(bool(winHelper2.cb) == false);

         resetTestObjs();
     }},

    {CASE("Canvas move assignment must move callback (unregister/register)")
     {
         XWinTestHelper winHelper1;
         XWinWrap::helperPtr = &winHelper1;
         Canvas *movedFrom = newTestObj();
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{seqBuf.data(), seqBuf.size()};
         *movedFrom += new TestShape{1, &seq};
         *movedFrom += new TestShape{2, &seq};

         XWinTestHelper winHelper2;
         XWinWrap::helperPtr = &winHelper2;
         Canvas movedTo{-1, -2, std::string()};

         std::array<GuiFuncs, 12> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> functSeq(functionSeq.data(), functionSeq.size());
         winHelper1.seqObj = &functSeq;

         movedTo = std::move(*movedFrom);

#ifndef HAS_CANVAS_PIMPL
         EXPECT(functionSeq[0] == unregisterCb);
         EXPECT(functionSeq[1] == registerCb);
#endif

         // call moved callback
         winHelper1.cb();
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         EXPECT(seqBuf == newDrawSequence);

         resetTestObjs();
     }},


    {CASE("Canvas move assignment must properly clean up old target (left operand)")
     {
         Canvas *movedFrom = newTestObj();

         XWinTestHelper winHelper2;
         XWinWrap::helperPtr = &winHelper2;

         CairoWrap testCairo;
         XWinWrap::cairoWrapPtr = &testCairo;

         Canvas *movedTo = newTestObj();
         auto s = new TestShape;
         bool oldShapeAssignedOverDestructed = false;
         s->destructionMarker = &oldShapeAssignedOverDestructed;
         *movedTo += s;

         std::array<GuiFuncs, 12> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> functSeq(functionSeq.data(), functionSeq.size());
         winHelper2.seqObj = &functSeq;
         testCairo.seq = &functSeq;

         *movedTo = std::move(*movedFrom);

         std::array<GuiFuncs, 12> funcSequence{unregisterCb
                                               , cairoDestroy
                                               , surfaceDestroy
                                               , winClean};
         EXPECT(functionSeq == funcSequence);
         EXPECT(oldShapeAssignedOverDestructed == true);

         resetTestObjs();
     }},

    {CASE("Canvas destructor of moved-to after move assignment must properly clean up")
     {
         Canvas *movedFrom = newTestObj();
         *movedFrom += new TestShape;

         Canvas *movedTo = newTestObj();

         *movedTo = std::move(*movedFrom);

         std::array<GuiFuncs, 12> functionSeq{GuiFuncs(0)};
         Sequencer<GuiFuncs> fSeq(functionSeq.data(), functionSeq.size());
         globalGuiSeq = &fSeq;

         movedTo->~Canvas();

         std::array<GuiFuncs, 12> funcSequence{unregisterCb
                                               , shapeClean
                                               , cairoDestroy
                                               , surfaceDestroy
                                               , winClean};
         EXPECT(functionSeq == funcSequence);

         resetTestObjs();

         // restore movedTo to a destructible state
         new (movedTo) Canvas(0, 0, std::string());
     }},

    {CASE("Check full Canvas move assignment sequence")
     {
         //trompeloeil::stream_tracer log(std::cout);
         unsigned char cbuf1[sizeof(Canvas)];
         Canvas *c1{static_cast<Canvas *>(static_cast<void *>(cbuf1))};

         unsigned char cbuf2[sizeof(Canvas)];
         Canvas *c2{static_cast<Canvas *>(static_cast<void *>(cbuf2))};

         GuiWinMock testWin;
         GuiWinWrap const * win1{nullptr};
         GuiWinWrap const * win2{nullptr};
         GuiWinWrap const * win3{nullptr};

         CairoMock testCairo;
         CairoMock *cp = &testCairo;

         auto testShape1 = new ShapeMock;
         auto testShape2 = new ShapeMock;
         std::function<void()> cb1{nullptr};
         std::function<void()> cb2{nullptr};
         std::function<void()> cb3{nullptr};

         trompeloeil::sequence seq, seq2;

         {
             REQUIRE_CALL(testWin, create(_))
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, getSurface())
                 .RETURN(cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testCairo, cairo_create(_))
                 .WITH(_1 == cp)
                 .RETURN(cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_SIDE_EFFECT(win1 = _1)
                 .LR_SIDE_EFFECT(cb1 = _2)
                 .IN_SEQUENCE(seq);

             new (c1) Canvas{77, -77, "C1"};
             *c1 += testShape1;
         }
         EXPECT(testOk());

         CairoWrap testCairo2;
         CairoWrap *cp2 = &testCairo2;

         {
             REQUIRE_CALL(testWin, create(_))
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, getSurface())
                 .RETURN(cp2)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testCairo, cairo_create(_))
                 .WITH(_1 == cp2)
                 .RETURN(cp2)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_SIDE_EFFECT(win2 = _1)
                 .LR_SIDE_EFFECT(cb2 = _2)
                 .IN_SEQUENCE(seq);

             new (c2) Canvas{8, -8, "C2"};
             *c2 += testShape2;
         }
         EXPECT(testOk());
         EXPECT(win2 != win1);

         {
             REQUIRE_CALL(*testShape1, doDraw(_))
                 .WITH(_1 == cp);

             cb1();
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape2, doDraw(_))
                 .WITH(_1 == cp2);

             cb2();
         }
         EXPECT(testOk());

         {
             bool win2r = true;

             FORBID_CALL(testWin, create(_));
             FORBID_CALL(testWin, getSurface());
             FORBID_CALL(testCairo, cairo_create(_));

             // I don't require a sequence here, as the rule-of-zero
             // approach does it in the wrong order.
             // and for cairo it's ok.
             REQUIRE_CALL(testCairo, cairo_destroy(_));
                 //.WITH(_1 == cp2);

             REQUIRE_CALL(testCairo, cairo_surface_destroy(_))
                 .WITH(_1 == cp2);

             // unregister the old c2
             // possibly repeatedly register and unregister the old c2
             // but we require below that it finishes unregistered
             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .WITH(_1 == win2)
                 .LR_WITH(win2r == false)
                 .LR_SIDE_EFFECT(win2r = true);

             REQUIRE_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win2)
                 .TIMES(AT_LEAST(1))
                 .LR_SIDE_EFFECT(cb2 = nullptr;)
                 .LR_SIDE_EFFECT(win2r = false)
                 .IN_SEQUENCE(seq, seq2);

             REQUIRE_CALL(testWin, clean(_))
                 .WITH(_1 == win2)
                 .IN_SEQUENCE(seq);

             // unregister the old c1 and re-register the new c1
             ALLOW_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win1)
                 .LR_SIDE_EFFECT(cb1 = nullptr;)
                 .IN_SEQUENCE(seq2);

             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_WITH(cb1 == nullptr)
                 .LR_SIDE_EFFECT(win3 = _1)
                 .LR_SIDE_EFFECT(cb3 = _2)
                 .IN_SEQUENCE(seq2);

             *c2 = std::move(*c1);
             EXPECT(win2r == false);

             if (cb1) // we may have pimpl
             {
                 cb3 = cb1;
                 cb1 = nullptr;
                 win3 = win1;
             }
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape1, doDraw(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             cb3();
         }
         EXPECT(testOk());

         {
             FORBID_CALL(testWin, unregisterCb(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             c1->~Canvas();
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape1, doDraw(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             cb3();
         }
         EXPECT(testOk());

         // check self assignment
         {
             // while we want self assignment, we don't require it's a no-op
             FORBID_CALL(testWin, create(_));
             FORBID_CALL(testWin, getSurface());
             FORBID_CALL(testCairo, cairo_create(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             ALLOW_CALL(testWin, unregisterCb(_));
             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)));

             *c2 = std::move(*c2);
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape1, doDraw(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             cb3();
         }
         EXPECT(testOk());


         {
             REQUIRE_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win3)
                 .LR_SIDE_EFFECT(cb3 = nullptr;)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testCairo, cairo_destroy(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testCairo, cairo_surface_destroy(_))
                 .WITH(_1 == cp)
                 .IN_SEQUENCE(seq);

             REQUIRE_CALL(testWin, clean(_))
                 .IN_SEQUENCE(seq);

             c2->~Canvas();
         }

         EXPECT(testOk());
     }},

    // this test case is only partly required:
    // assignment into moved-from is generally required,
    // but moving a moved-from again is not!
    {CASE("Check correct moved-from assignment handling for Canvas")
     {
         //trompeloeil::stream_tracer log(std::cout);
         unsigned char cbuf1[sizeof(Canvas)];
         Canvas *c1{static_cast<Canvas *>(static_cast<void *>(cbuf1))};

         unsigned char cbuf2[sizeof(Canvas)];
         Canvas *c2{static_cast<Canvas *>(static_cast<void *>(cbuf2))};

         unsigned char cbuf3[sizeof(Canvas)];
         Canvas *c3{static_cast<Canvas *>(static_cast<void *>(cbuf3))};

         GuiWinMock testWin;
         auto testShape = new ShapeMock;
         GuiWinWrap const * win1{nullptr};
         GuiWinWrap const * win2{nullptr};
         GuiWinWrap const * win3{nullptr};

         std::function<void()> cb1{nullptr};
         std::function<void()> cb2{nullptr};
         std::function<void()> cb3{nullptr};

         CairoMock testCairo;
         CairoMock *cp = &testCairo;

         // setup and allow everything (already tested before)
         {
             ALLOW_CALL(testWin, create(_));
             ALLOW_CALL(testWin, getSurface())
                 .RETURN(cp);
             ALLOW_CALL(testCairo, cairo_create(_))
                 .RETURN(cp);
             REQUIRE_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_SIDE_EFFECT(win1 = _1)
                 .LR_SIDE_EFFECT(cb1 = _2);

             new (c1) Canvas{77, -77, "C"};
             *c1 += testShape;
         }
         EXPECT(testOk());

         {
             ALLOW_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win1)
                 .LR_SIDE_EFFECT(cb1 = nullptr);
             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_WITH(cb1 == nullptr)
                 .LR_SIDE_EFFECT(win2 = _1)
                 .LR_SIDE_EFFECT(cb2 = _2);

             new (c2) Canvas{std::move(*c1)};
             if (cb1) // we may have pimpl
             {
                 cb2 = cb1;
                 cb1 = nullptr;
                 win2 = win1;
             }
         }
         EXPECT(testOk());

         // move moved-from again
         // this possibly just crashes in Canvas ctor if handled wrongly
         {
             FORBID_CALL(testWin, registerCb(_, ANY(std::function<void()>)));
             FORBID_CALL(testWin, unregisterCb(_));

             new (c3) Canvas{std::move(*c1)};
         }
         EXPECT(testOk());

         {
             FORBID_CALL(testWin, registerCb(_, ANY(std::function<void()>)));
             FORBID_CALL(testWin, unregisterCb(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             c3->~Canvas();
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape, doDraw(_))
                 .WITH(_1 == cp);

             cb2();
         }
         EXPECT(testOk());

         // move valid obj into moved-from
         {
             ALLOW_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win2)
                 .LR_SIDE_EFFECT(cb2 = nullptr);
             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .LR_WITH(cb2 == nullptr)
                 .LR_SIDE_EFFECT(win3 = _1)
                 .LR_SIDE_EFFECT(cb3 = _2);

             *c1 = std::move(*c2);
             if (cb2) // we may have pimpl
             {
                 cb3 = cb2;
                 cb2 = nullptr;
                 win3 = win2;
             }
         }
         EXPECT(testOk());

         {
             REQUIRE_CALL(*testShape, doDraw(_))
                 .WITH(_1 == cp);

             cb3();
         }
         EXPECT(testOk());

         // move moved-from into valid obj
         {
             // we allow 'Canvas tmp(std::move(*this)'
             bool win3r = true;

             ALLOW_CALL(testWin, registerCb(_, ANY(std::function<void()>)))
                 .WITH(_1 == win3)
                 .LR_WITH(win3r == false)
                 .LR_SIDE_EFFECT(win3r = true);

             ALLOW_CALL(testWin, unregisterCb(_))
                 .WITH(_1 == win3)
                 .LR_WITH(win3r == true)
                 .LR_SIDE_EFFECT(win3r = false);

             REQUIRE_CALL(testCairo, cairo_destroy(_));
             REQUIRE_CALL(testCairo, cairo_surface_destroy(_));
             REQUIRE_CALL(testWin, clean(_));

             *c1 = std::move(*c2);
             EXPECT(win3r == false);
         }
         EXPECT(testOk());

         // move moved-from into moved-from
         {
             FORBID_CALL(testWin, unregisterCb(_));
             FORBID_CALL(testCairo, cairo_destroy(_));
             FORBID_CALL(testCairo, cairo_surface_destroy(_));
             FORBID_CALL(testWin, clean(_));

             *c1 = std::move(*c2);
         }
         EXPECT(testOk());

         c1->~Canvas();
         c2->~Canvas();
         EXPECT(testOk());
     }},

#ifndef _WIN32
    // memory calls are ok (for std::function in registerCallback)
    // but they must add up to zero
    {CASE("Canvas move assignment w/ empty target must not cause any memory overhead")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management

             Canvas c1{2, 55, "tc1"};
             // we don't have a default ctor for Canvas
             Canvas c2{7, 13, "tc2"};
             Canvas c3{std::move(c2)}; // now c2 is empty

             auto memCnt = testAlloc.getCount();
             c2 = std::move(c1);
             EXPECT(memCnt == testAlloc.getCount());

             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},

    {CASE("Canvas move assignment must release old target")
     {
         AllocMock testAlloc;
         int cTestMem = 0;
         {
             Canvas cTest{2, 55, "tc1"};

             {
                 cTestMem = testAlloc.getCount();
                 Canvas c2{7, 13, "tc2"};
                 TestShape *s(new TestShape{"Test2", Position{150, 70}});
                 bool oldShapeDestructed = false;
                 s->destructionMarker = &oldShapeDestructed;
                 c2 += s;

                 c2 = std::move(cTest); // here old c2 should get cleaned up

                 EXPECT(oldShapeDestructed == true);
                 EXPECT(testAlloc.getCount() == cTestMem);
             } // here new c2 (old cTest) should get cleaned up
             EXPECT(testAlloc.getCount() == 0);
         }
         EXPECT(testAlloc.ok());
     }},

    {CASE("Canvas move assignment must move ownership (no destruction of moved from)")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management

             Canvas c1{2, 55, "tc1"};
             // we don't have a default ctor for Canvas
             Canvas c2{7, 13, "tc2"};
             Canvas c3{std::move(c2)}; // now c2 is empty

             auto memCnt = testAlloc.getCount();

             Canvas tmp{std::move(c2)};
             c2 = std::move(c1);
             c1 = std::move(tmp);
             tmp.~Canvas();
             new (&tmp) Canvas{std::move(c1)};

             EXPECT(memCnt == testAlloc.getCount());

             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},
#endif
#endif // HAS_CANVAS_MOVE_ASSIGNMENT
#ifdef HAS_CANVAS_GET_SHAPE
    {CASE("Check Canvas getShape() for moved elements")
     {
         Canvas c{2, 23, "c"};
         auto t1 = new Text{"Hello", "Text1"};
         auto t2 = new Text{"World", "Text2"};

         c += t1;
         EXPECT(&(c.getShape("Text1")->get()) == t1);
         EXPECT(c.getShape("Text2").has_value() == false);

         *t1 = std::move(*t2);
         EXPECT(&(c.getShape("Text2")->get()) == t1);
         EXPECT(c.getShape("Text1").has_value() == false);
     }},
#endif // HAS_CANVAS_GET_SHAPE
};
} // unnamed namespace

int main(int argc, char *argv[])
{
    testSetup();
    exerciseTest::setTromploeilReporter();

    int status = lest::run(canvasTests, argc, argv, std::cerr);

    if (status == 0)
    {
        // cleanup all test objects
        for (Canvas *p: testObjs)
        {
            delete p;
        }
    }

    return status;
}
