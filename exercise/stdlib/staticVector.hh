#ifndef STATIC_VECTOR_HH_SEEN
#define STATIC_VECTOR_HH_SEEN
/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cstddef>

namespace exercise
{
template <class T>
class StaticVector
{
public:
    typedef T value_type;
    typedef T *iterator;
    typedef T const *const_iterator;

    explicit StaticVector(size_t size);
    StaticVector(StaticVector const &rhs);
    StaticVector(StaticVector &&rhs);

    ~StaticVector();

    StaticVector &operator=(StaticVector tmp); // universal assignment

    T &operator[](size_t idx);
    T const &operator[](size_t idx) const;

    template <typename U> void push_back(U &&u);
    void pop_back();

    iterator erase(iterator start, iterator last);

    size_t size() const;

    bool equal(StaticVector const &other) const;

private:
    size_t dataSize;
    size_t realSize = 0;
    T *data;
};

template <class T>
bool operator==(StaticVector<T> const &, StaticVector<T> const &);
} // namespace exercise
#endif /* STATIC_VECTOR_HH_SEEN */
