/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include "staticVector.hh"
#include <iostream>
#include <vector>
#include <set>
#include "lest.hpp"

using std::cout;
using namespace std::literals;

#if __has_include("staticVector.tt")
#include "staticVector.tt"
#endif

typedef std::vector<int> IntVect;
//typedef exercise::StaticVector<int> IntVect;

namespace exercise
{
auto minmax(IntVect const &v);

int sum(IntVect const &v);

double avg(IntVect const &v);

void delOdd(IntVect &v);

std::set<int> asSet(IntVect const &v);

void delEven(std::set<int> &si);

template <typename Cont>
void printCont(Cont const &c, std::ostream &os = cout);
} // namespace exercise

#include "test-stdlib.hh"

int main()
{
    using namespace exercise;

    int status = lest::run(exerciseTest::myTests, exerciseTest::lestArgs, std::cout);
    if (status == 0)
    {
        cout << "Tests successful\n";
    } else {
        cout << "Tests failed\n";
    }


    IntVect vi{ 22, 33, 44, 12, 2, 18, 4, 27, 44, 91 };

    cout << "Original vector: ";
    printCont(vi);

    auto mm = minmax(vi);
    cout << "Minimum: " << mm.first << ", Maximum: " << mm.second << '\n';

    cout << "Total: " << sum(vi) << '\n';
    
    cout << "Average: " << avg(vi) << '\n';

    std::set<int> si = asSet(vi);

    delOdd(vi);
    cout << "Vector without odd numbers: ";
    printCont(vi);

    cout << "Original set:\n";
    printCont(si);

    delEven(si);
    cout << "Set without even numbers: ";
    printCont(si);

    return 0;
}
