/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
namespace exerciseTest
{
using namespace exercise;
std::vector lestArgs = {"-p"s, "-a"s};
IntVect vTest1{-3};
IntVect vTest2{1, 2, 3, 2};
const lest::test myTests[] =
{
    CASE("Check IntVect::size()")
    {
        EXPECT(vTest1.size() == 1ul);
        EXPECT(vTest2.size() == 4ul);
    },
    CASE("Check IntVect::begin()")
    {
        EXPECT(*vTest1.begin() == -3);
        EXPECT(*vTest2.begin() == 1);
    },
    CASE("Check IntVect::end()")
    {
        EXPECT(vTest1.end() == vTest1.begin()+vTest1.size());
        EXPECT(vTest2.end() == vTest2.begin()+vTest2.size());
    },
    CASE("Check range based for on IntVect")
    {
        IntVect v(4);
        for (int elem: vTest2)
        {
            v.push_back(elem);
        }
        EXPECT(vTest2 == v);
    },
    CASE("Check minmax on single element")
    {
        auto result = exercise::minmax(vTest1);

        EXPECT(result.first == -3);
        EXPECT(result.second == -3);
    },
    CASE("Check minmax normal")
    {
        auto result = minmax(vTest2);

        EXPECT(result.first == 1);
        EXPECT(result.second == 3);
    },
    CASE("Check sum on single element")
    {
        int result = sum(vTest1);

        EXPECT(result == -3);
    },
    CASE("Check sum normal")
    {
        int result = sum(vTest2);

        EXPECT(result == 8);
    },
    CASE("Check avg on single element")
    {
        int result = avg(vTest1);

        EXPECT(result == -3);
    },
    CASE("Check avg normal")
    {
        int result = avg(vTest2);

        EXPECT(result == 2);
    },
    CASE("Check delOdd")
    {
        IntVect vTest = vTest2;
        IntVect expected{ 2, 2 };
        delOdd(vTest);

        EXPECT(vTest.size() == 2u);
        EXPECT(vTest == expected);
    },
    CASE("Check asSet")
    {
        std::set<int> sTest = asSet(vTest2);
        std::set<int> expected{ 1, 2, 3 };

        EXPECT(sTest.size() == 3u);
        EXPECT(sTest == expected);
    },
    CASE("Check delEven")
    {
        std::set<int> sTest{ 2, 4, 6, 7, 8 };
        std::set<int> expected{ 7 };
        delEven(sTest);

        EXPECT(sTest.size() == 1u);
        EXPECT(sTest == expected);
    },
};
} // namespace exerciseTest

